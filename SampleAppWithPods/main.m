//
//  main.m
//  SampleAppWithPods
//
//  Created by Herve Peroteau on 26/11/2018.
//  Copyright © 2018 Herve Peroteau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
